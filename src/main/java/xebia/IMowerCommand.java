package xebia;

/**
 * this is a behaviour interface of a mower
 * Created by emmanuel on 07/09/2015.
 */
public interface IMowerCommand {

    /**
     * just turn it left
     */

    public void turnLeft();

    /**
     * just turn it right
     */

    public void turnRight();

    /**
     * to move front
     */
    public void goAhead();

    /**
     * this will be used to abort the last action performed
     *
     */
    public void abort();

    /**
     * this will save the last action performed
     */
    public void commit();

    public Mower getMower();


    /**
     * this will be use by the engine to check the next mower coodinate
     * before to commit or to abort
     *
     * @return The next Mower coordinate
     */
    public MowerCoordinate getNextMowerCoordinate();
}
