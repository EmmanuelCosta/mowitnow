package xebia;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * this is a mow it now file format dao
 * Created by emmanuel on 09/09/2015.
 */
public class MowItNowFromFileDAO implements MowItNowDAO {
    private final BufferedReader bufferedReader;
    private boolean isFirstLineRead = false;


    public MowItNowFromFileDAO(String path) throws FileNotFoundException {
        Objects.requireNonNull(path);
        File file = new File(path);
        this.bufferedReader = new BufferedReader(new FileReader(file));
    }


    public Map<String, String> getNextMowerPositionAndInstruction() throws IllegalStateException, IOException {
        if (!this.isFirstLineRead) {
            throw new IllegalStateException("You can not call this method before retreiving the top right coordinate");
        }
        String coordinate = this.bufferedReader.readLine();
        //if it null that means there is no more thing to read
        if (Objects.isNull(coordinate)) {
            this.bufferedReader.close();
            return null;
        }
        String actions = this.bufferedReader.readLine();
        //if it null it means that the file did not respect the standard
        if (Objects.isNull(actions)) {
            throw new IllegalStateException("Exception here means that your file format is wrong. Please check that you have for each mower coordinate a mower instructions.");
        }
        Map<String, String> mowerElementsFromFile = new HashMap<String, String>();
        mowerElementsFromFile.put("coordinate", coordinate);
        mowerElementsFromFile.put("action", actions);
        return mowerElementsFromFile;
    }

    public String getTopRightCoordinate() throws IllegalStateException, IOException {
        if (Objects.isNull(this.bufferedReader)) {
            throw new IllegalStateException("The connexion must be open. Please check that the patrh you give to the file is right");
        } else if (this.isFirstLineRead) {
            throw new IllegalStateException("The top right coordinate has already been read. you can not call this method twice.");
        }
        this.isFirstLineRead = true;

        return this.bufferedReader.readLine();
    }
}
