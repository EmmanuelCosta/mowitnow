package xebia;

import java.io.IOException;
import java.util.Map;

/**
 * this must be implement by all object which have to give access to data
 * Created by emmanuel on 09/09/2015.
 */
public interface MowItNowDAO {


    /**
     * this will return a map containing coordinate of the Map and Action it must perform
     * the key for coordinate is coordinate
     * the key for actions is action
     * <p>
     * It works like a cursor
     * every call will give back a mapping for the next mower
     *
     * @return
     * @throws IllegalStateException
     * @throws IOException
     */
    public Map<String, String> getNextMowerPositionAndInstruction() throws IllegalStateException, IOException;

    /**
     * @return the top right coordinate
     * @throws IllegalStateException
     * @throws IOException
     */
    public String getTopRightCoordinate() throws IllegalStateException, IOException;


}
