package xebia;

import java.util.Objects;

/**
 * This is a mower class
 * it's  the object we interact with
 * Created by emmanuel on 07/09/2015.
 */
public class Mower {

    private final IMowerCommand mower_command;
    private MowerCoordinate current_mower_coordinate;


    public Mower(int x, int y, MowerCardinalOrientation mowerCardinalOrientation) {
        Objects.requireNonNull(mowerCardinalOrientation);
        this.current_mower_coordinate = new MowerCoordinate(x, y, mowerCardinalOrientation);
        this.mower_command = new MowerCommand(this);
    }



    public int getX_coordinate() {
        return current_mower_coordinate.getX_coordinate();
    }

    public int getY_coordinate() {
        return current_mower_coordinate.getY_coordinate();
    }

    public MowerCardinalOrientation getMowerCardinalOrientation() {
        return current_mower_coordinate.getMowerCardinalOrientation();
    }

    public MowerCoordinate getCurrentMowerCoordinate() {
        return current_mower_coordinate;
    }

    public IMowerCommand getMowerCommand() {
        return mower_command;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(current_mower_coordinate.getX_coordinate() + " ");
        sb.append(current_mower_coordinate.getY_coordinate() + " ");
        sb.append(current_mower_coordinate.getMowerCardinalOrientation() + " ");
        return sb.toString();
    }
}
