package xebia;

/**
 * Created by emmanuel on 07/09/2015.
 */
public enum MowerCardinalOrientation {
    N(0), E(1), S(2), O(3);

    private int value;

    private MowerCardinalOrientation(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }


    public static String getEnumCardinalCoordinate(int value) {
        for (MowerCardinalOrientation mowerCardinalOrientation : MowerCardinalOrientation.values()) {

            if (value == mowerCardinalOrientation.value) {
                return mowerCardinalOrientation.name();
            }
        }
        throw new IllegalArgumentException("The value provided is wrong must be one of those : {0,1,2,3} ");
    }
}
