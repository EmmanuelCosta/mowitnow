package xebia;

import java.util.Objects;

/**
 * This class represent the behaviour of our mower
 * the way it move to the left , to the right or to go ahead
 * Created by emmanuel on 07/09/2015.
 */
public class MowerCommand implements IMowerCommand {
    private final Mower mower;
    private MowerCoordinate next_mower_coordinate;

    public MowerCommand(Mower mower) {
        Objects.requireNonNull(mower);
        this.mower = mower;
        this.next_mower_coordinate = new MowerCoordinate(mower.getX_coordinate(), mower.getY_coordinate(), mower.getMowerCardinalOrientation());
    }

    public void turnLeft() {
        int value = this.next_mower_coordinate.getMowerCardinalOrientation().getValue();
        int nextValue = (((value - 1) % 4) + 4) % 4;
       
        String enumCardinalCoordinate = MowerCardinalOrientation.getEnumCardinalCoordinate(nextValue);
        this.next_mower_coordinate.setMowerCardinalOrientation(MowerCardinalOrientation.valueOf(enumCardinalCoordinate));
    }

    public void turnRight() {
        int value = this.next_mower_coordinate.getMowerCardinalOrientation().getValue();
        int nextValue = (value + 1) % 4;
        String enumCardinalCoordinate = MowerCardinalOrientation.getEnumCardinalCoordinate(nextValue);
        this.next_mower_coordinate.setMowerCardinalOrientation(MowerCardinalOrientation.valueOf(enumCardinalCoordinate));
    }

    public void goAhead() {
        int value = this.next_mower_coordinate.getMowerCardinalOrientation().getValue();
        int x = this.next_mower_coordinate.getX_coordinate();
        int y = this.next_mower_coordinate.getY_coordinate();
        switch (value) {
            case 0:
                y++;
                next_mower_coordinate.setY_coordinate(y);
                break;
            case 1:
                x++;
                next_mower_coordinate.setX_coordinate(x);
                break;
            case 2:
                y--;
                next_mower_coordinate.setY_coordinate(y);
                break;

            case 3:
                x--;
                next_mower_coordinate.setX_coordinate(x);
                break;

        }


    }

    public void abort() {
        int x = this.mower.getX_coordinate();
        int y = this.mower.getY_coordinate();
        MowerCardinalOrientation mowerCardinalOrientation = this.mower.getMowerCardinalOrientation();
        this.next_mower_coordinate.setX_coordinate(x);
        this.next_mower_coordinate.setY_coordinate(y);
        this.next_mower_coordinate.setMowerCardinalOrientation(mowerCardinalOrientation);
    }

    public void commit() {
        int x = this.next_mower_coordinate.getX_coordinate();
        int y = this.next_mower_coordinate.getY_coordinate();
        MowerCardinalOrientation mowerCardinalOrientation = this.next_mower_coordinate.getMowerCardinalOrientation();
        MowerCoordinate currentMowerCoordinate = this.mower.getCurrentMowerCoordinate();
        currentMowerCoordinate.setX_coordinate(x);
        currentMowerCoordinate.setY_coordinate(y);
        currentMowerCoordinate.setMowerCardinalOrientation(mowerCardinalOrientation);

    }

    public Mower getMower() {
        return this.mower;
    }

    public MowerCoordinate getNextMowerCoordinate() {
        return this.next_mower_coordinate;
    }
}
