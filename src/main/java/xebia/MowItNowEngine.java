package xebia;

import java.io.*;
import java.util.Map;
import java.util.Objects;

/**
 * This class will manage the mower and move it according to the data retreive from file
 * It maintain also the integrity of the mower data i.e valid coordinate according to the area
 * So it command the mower and valid or abort a move
 * <p>
 * Created by emmanuel on 07/09/2015.
 */
public class MowItNowEngine {

    private int min_x_coordianate = 0;
    private int min_y_coordianate = 0;
    private int max_x_coordianate = 0;
    private int max_y_coordianate = 0;


    /**
     * this will read a MowItNow file format  and perform action according to the data read
     *
     * @param path
     * @throws IOException
     */
    public void processFromFile(String path) throws IOException {
        Objects.requireNonNull(path);

        MowItNowDAO mowItNowDAO = new MowItNowFromFileDAO(path);
        String instruction = mowItNowDAO.getTopRightCoordinate();
        readMaxSizeCoordinate(instruction);
        Map<String, String> nextMowerPositionAndInstruction = null;

        while ((nextMowerPositionAndInstruction = mowItNowDAO.getNextMowerPositionAndInstruction()) != null) {
            instruction = nextMowerPositionAndInstruction.get("coordinate");
            Mower mower = getMowerFromFile(instruction);
            instruction = nextMowerPositionAndInstruction.get("action");
            performMowerCommand(mower, instruction);
            System.out.println(mower);
        }
    }

    /**
     * this will initiate a mower from coordinate read from file
     *
     * @param mowerFile
     * @return
     */
    public Mower getMowerFromFile(String mowerFile) throws IOException {
        Objects.requireNonNull(mowerFile);

        String[] split = mowerFile.split(" ");
        Integer x = new Integer(split[0]);
        Integer y = new Integer(split[1]);
        String cardinal = split[2];

        if (!isCoordinateValid(x, y, cardinal)) {
            throw new IllegalArgumentException("Please check the initial coordinate." +
                    " The cordinate x or y must be wrong because smaller than the min size or greater than the max size of the area to mow");
        }
        MowerCardinalOrientation mowerCardinalOrientation = MowerCardinalOrientation.valueOf(cardinal);
        return new Mower(x, y, mowerCardinalOrientation);
    }

    public boolean isCoordinateValid(int x, int y, String cardinalOrientation) {
        if (x < getMin_x_coordinate() || y < getMin_y_coordinate() || x > getMax_x_coordinate() || y > getMax_y_coordinate()) {
            return false;
        }
        MowerCardinalOrientation[] values = MowerCardinalOrientation.values();
        for (MowerCardinalOrientation mowerCardinalOrientation : values) {
            if (mowerCardinalOrientation.name().equals(cardinalOrientation)) {
                return true;
            }
        }
        return true;
    }

    /**
     * this will perform action read from file
     *
     * @param mower
     */
    public void performMowerCommand(Mower mower, String mowerActionFile) throws IOException {
        Objects.requireNonNull(mower);
        Objects.requireNonNull(mowerActionFile);

        IMowerCommand mowerCommand = mower.getMowerCommand();
        for (char c : mowerActionFile.toCharArray()) {
            switch (c) {
                case 'G':
                    mowerCommand.turnLeft();
                    break;
                case 'D':
                    mowerCommand.turnRight();
                    break;
                case 'A':
                    mowerCommand.goAhead();
                    break;
            }


            MowerCoordinate nextMowerCoordinate = mowerCommand.getNextMowerCoordinate();
            boolean result = isCoordinateValid(nextMowerCoordinate.getX_coordinate(), nextMowerCoordinate.getY_coordinate(), nextMowerCoordinate.getMowerCardinalOrientation().name());
            if (result) {
                mowerCommand.commit();
            } else {
                mowerCommand.abort();
            }

        }
    }

    /**
     * this will set the max x and y coordinate of the area
     *
     * @param maxSizeCoordinate
     * @throws IOException
     */
    public void readMaxSizeCoordinate(String maxSizeCoordinate) throws IOException {
        Objects.requireNonNull(maxSizeCoordinate);

        if (maxSizeCoordinate.equals(" ")) {
            throw new IllegalStateException("The first line cannot be empty. Please check your file format");
        }
        String[] split = maxSizeCoordinate.split(" ");
        max_x_coordianate = new Integer(split[0]);
        max_y_coordianate = new Integer(split[1]);
    }


    public int getMin_x_coordinate() {
        return min_x_coordianate;
    }

    public int getMin_y_coordinate() {
        return min_y_coordianate;
    }

    public int getMax_x_coordinate() {
        return max_x_coordianate;
    }

    public int getMax_y_coordinate() {
        return max_y_coordianate;
    }
}
