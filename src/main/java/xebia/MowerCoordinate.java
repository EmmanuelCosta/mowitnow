package xebia;

/**
 * Created by emmanuel on 07/09/2015.
 */
public class MowerCoordinate {
    private int x_coordinate;
    private int y_coordinate;
    private MowerCardinalOrientation mower_cardinal_orientation;


    public MowerCoordinate(int x_coordinate, int y_coordinate, MowerCardinalOrientation mower_cardinal_orientation) {
        this.x_coordinate = x_coordinate;
        this.y_coordinate = y_coordinate;
        this.mower_cardinal_orientation = mower_cardinal_orientation;
    }

    public int getX_coordinate() {
        return x_coordinate;
    }

    public void setX_coordinate(int x_coordinate) {
        this.x_coordinate = x_coordinate;
    }

    public int getY_coordinate() {
        return y_coordinate;
    }

    public void setY_coordinate(int y_coordinate) {
        this.y_coordinate = y_coordinate;
    }

    public MowerCardinalOrientation getMowerCardinalOrientation() {
        return mower_cardinal_orientation;
    }

    public void setMowerCardinalOrientation(MowerCardinalOrientation mower_cardinal_orientation) {
        this.mower_cardinal_orientation = mower_cardinal_orientation;
    }
}
