package xebia;


import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;


/**
 * Created by emmanuel on 07/09/2015.
 */
public class MowItNowTest {

    /**
     * this will test the result of mowitnow by injected this data
     * <<
     * 5 5
     * 1 2 N
     * GAGAGAGAA
     * 3 3 E
     * AADAADADDA
     * >>
     * the data is normally given via a file
     * but for the testing we will inject data using mock object
     *
     * @throws IOException
     */
    @Test
    public void testMowItNow() throws IOException {

        // The first line give the top right coordinate
        // this coordinate is (5,5)
        MowItNowEngine mowItNowEngine = new MowItNowEngine();

        mowItNowEngine.readMaxSizeCoordinate("5 5");

        //the next line indicate the coordinate of a mower
        //his position is (1,2,N)

        Mower firstMower = mowItNowEngine.getMowerFromFile("1 2 N");

        //the next line indicate a series of action the mower must perform
        // the task to perform here is GAGAGAGAA
        mowItNowEngine.performMowerCommand(firstMower, "GAGAGAGAA");

        //now we check that we have finally the coordinate (1,3,N)
        Assert.assertEquals(1, firstMower.getX_coordinate());
        Assert.assertEquals(3, firstMower.getY_coordinate());
        Assert.assertEquals("N", firstMower.getMowerCardinalOrientation().name());

        //the next line indicates the coordinate of another mower
        // his position is (3,3,E)
        Mower secondMower = mowItNowEngine.getMowerFromFile("3 3 E");

        //the next line indicate a series of action the second mower must perform
        // the task to perform here is AADAADADDA
        mowItNowEngine.performMowerCommand(secondMower, "AADAADADDA");

        //now we check that we have finally the coordinate (5,1,E)
        Assert.assertEquals(5, secondMower.getX_coordinate());
        Assert.assertEquals(1, secondMower.getY_coordinate());
        Assert.assertEquals("E", secondMower.getMowerCardinalOrientation().name());
    }
}